# Changelog

All notable changes to this project will be documented in this file. See
[Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.1.0](https://git.abfelbaum.dev/abfelbaum/examples/semantic-release/compare/2.0.0...2.1.0) (2022-12-12)


### Features

* **api:** AsdBsd ([e1ddff0](https://git.abfelbaum.dev/abfelbaum/examples/semantic-release/commit/e1ddff008b259a633f338442e175b8fe35f0793f))


### Bug Fixes

* Csd ([42dea82](https://git.abfelbaum.dev/abfelbaum/examples/semantic-release/commit/42dea824723d2f1275ef5203b7717fc6cf554902)), closes [abfelbaum/examples/semantic-release#1](https://git.abfelbaum.dev/abfelbaum/examples/semantic-release/issues/1)

## [2.0.0](https://git.abfelbaum.dev/abfelbaum/examples/semantic-release/compare/1.1.0...2.0.0) (2022-12-06)


### ⚠ BREAKING CHANGES

* asd

### Features

* bsd ([d42ce4c](https://git.abfelbaum.dev/abfelbaum/examples/semantic-release/commit/d42ce4c361bbe7bf8509260be9e9d8d85004f583))

## [1.1.0](https://git.abfelbaum.dev/abfelbaum/examples/semantic-release/compare/1.0.1...1.1.0) (2022-12-06)


### Features

* asd ([d8161f6](https://git.abfelbaum.dev/abfelbaum/examples/semantic-release/commit/d8161f676c42039d8af8c93618874263a9207f12))

## [1.0.1](https://git.abfelbaum.dev/abfelbaum/examples/semantic-release/compare/1.0.0...1.0.1) (2022-12-06)


### Bug Fixes

* ci ([56b32dc](https://git.abfelbaum.dev/abfelbaum/examples/semantic-release/commit/56b32dc980b0c7bdcb928a69af80097e4e23505d))

## 1.0.0 (2022-12-06)


### Features

* release ([48ef5e0](https://git.abfelbaum.dev/abfelbaum/examples/semantic-release/commit/48ef5e0f13d250c3ab711da72f1a38d0d908b370))
