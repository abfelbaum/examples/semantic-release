const branch = process.env.CI_COMMIT_BRANCH

const config = {
    tagFormat: "${version}",
    branches: [
        "+([0-9])?(.{+([0-9]),x}).x",
        "main",
        {
            name: "dev",
            channel: "default",
            prerelease: "beta"
        }
    ],
    plugins: [
        ["@semantic-release/commit-analyzer", {
            preset: "conventionalcommits"
        }],
        ["@semantic-release/release-notes-generator", {
            preset: "conventionalcommits"
        }],
        ["@semantic-release/gitlab", {
            labels: "Type::fix,From::Release Bot,Priority::High,State::Accepted"
        }]
    ]
}

if (config.branches.some(it => it === branch || (it.name === branch && !it.prerelease))) {

    config.plugins.push([ "@semantic-release/changelog", {
        changelogTitle: "# Changelog\n\nAll notable changes to this project will be documented in this file. See\n[Conventional Commits](https://conventionalcommits.org) for commit guidelines."
    }]);

    config.plugins.push([ "@semantic-release/git", {
        message: "chore(release): ${nextRelease.version}\n\n${nextRelease.notes}"
    }]);
}

if(branch !== "main" && branch !== "dev") {
    config.branches.push({
        name: branch,
        channel: "default",
        prerelease: "alpha"
    })
}

module.exports = config
